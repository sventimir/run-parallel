# run-parallel

run-parallel is a simple tool for running many instances of the same program in parallel.

# Get started

To actually run your program, you need to define a module describing how to do it.
For now two modules are pre-defined for running Tezos benchmarks with tezos-snoop
and to typecheck Tezos smart contracts with tezos-client. However, the tool is not
really related to Tezos anyhow (besides the fact it was developed to help Tezos
development process). It can be used to run any application whatsoever, as long as
it's non-interactive and communicates it's result in a parseable way.

# Build

To build the tool you need `dune`, `lwt` and `re` packages from Opam:

    $ opam install dune lwt re
    
Then:

    $ dune build
    
Finally to run:

    $ dune exec ./main.exe -- <number-of-parallel-processes>
    
Mind you that the program to be run as well as parameters for each run are
defined by the program-specific config module. Existing modules read the
tasks to perform from stdin, however user-defined modules can provide them
in a different fashion. Each task is just a string and the config module
describes how this string is integrated into the command spawning a new
process.

The configuration modules selected is hard-coded in the `main.ml` module,
so you need to change it and recompile in order to run a different program.

NOTE that you probably need to edit the user_`config.ml` module so that provided
paths for output files match your file system.
