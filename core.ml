let return = Lwt.return
let ( let* ) = Lwt.bind
let ( let+ ) m f = Lwt.map f m

type progress = {finished : int; total : int}

type event =
  | Worker_freed of int
  | Progress of int * progress

module type JOB = sig
  type task

  val show_task : task -> string
  
  val get_tasks : unit -> task list Lwt.t

  val total_progress : int
  
  val spawn_process : task -> Lwt_process.process_full

  val process_output :
    (event -> unit) ->
    task ->
    int ->
    Lwt_process.process_full ->
    unit ->
    unit Lwt.t
end

let ( // ) = Filename.concat

let home = Unix.getenv "HOME"

