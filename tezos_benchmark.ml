open Core
open User_config

type task = string

let show_task s = s

let executable = tezos // "tezos-snoop"

let bench_progress_re = Re.(compile @@ Perl.re "benchmarking ([0-9]+)/([0-9]+)")
let stats_re = Re.(compile @@ str "stats over all benchmarks")

let bench_num = 500
let nsamples = 500

let total_progress = bench_num

let get_tasks () =
  let* inp = Lwt_io.(read stdin) in
  let lines = String.split_on_char '\n' inp in
  Lwt.return
  @@ List.filter_map (fun l ->
         match String.split_on_char ':' l with
         | []
         | [""] -> None
         | [b]
         | (b :: _) -> Some b)
       lines

let spawn_process task =
  Lwt_process.open_process_full
    (executable, [|
       executable;
       "benchmark"; task;
       "and"; "save"; "to";
       Format.sprintf "%s/%s.workload" benchmark_results task;
       "--bench-num"; string_of_int bench_num;
       "--nsamples"; string_of_int nsamples;
       "--dump-csv"; Format.sprintf "%s/%s.csv" benchmark_results task;
     |])

let read_snoop chan =
  let rec read buf =
    let* chr_opt = Lwt_io.read_char_opt chan in
    match chr_opt with
    | Some chr when List.mem chr ['\n'; '\x0D'] ->
       return @@ Some buf
    | Some chr ->
       read @@ String.cat buf @@ String.make 1 chr
    | None when buf = "" ->
       return None
    | None ->
       return @@ Some buf
  in
  read ""

let process_output notify task job_id proc () =
  let proc_output output =
    match Re.exec_opt stats_re output with
    | Some _ ->
       Lwt_io.with_file
         ~mode:Output
         (Format.sprintf "%s/%s.out" benchmark_results task)
         (fun chan -> Lwt_io.write chan output)
    | None ->
       match Re.exec_opt bench_progress_re output with
       | Some g ->
          let progress = {
              finished = int_of_string @@ Re.Group.get g 1;
              total = int_of_string @@ Re.Group.get g 2;
            } in
          notify @@ Progress (job_id, progress);
          return ()
       | None ->
          return ()
  in
  let rec read () =
    let* output = read_snoop proc#stderr in
    match output with
    | None ->
       notify @@ Worker_freed job_id;
       return ()
    | Some o ->
       let* () = proc_output o in
       read ()
  in
  read ()
