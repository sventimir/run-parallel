open Core

let work_dir = home // "work"
let smart_contracts = work_dir // "contracts"
let typechecking_results = work_dir // "typechecking"
let benchmark_results = work_dir // "benchmarks"
let tezos = work_dir // "tezos"
