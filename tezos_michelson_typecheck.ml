open Core
open User_config

type task = string

let show_task = Filename.basename

(* val get_tasks : unit -> string list Lwt.t *)
let get_tasks () =
  Lwt_stream.to_list @@ Lwt_io.(read_lines stdin)

(* val total_progress : int *)
let total_progress = 1

let executable = tezos // "tezos-client"
  
(* val spawn_process : string -> Lwt_process.process_full *)
let spawn_process filename =
  Lwt_process.open_process_full
    (executable,
     [| executable; "-M"; "mockup"; "typecheck"; "script"; filename; "-q" |])

(* val process_output : *)
(*   (event -> unit) -> *)
(*   string -> *)
(*   int -> *)
(*   Lwt_process.process_full -> *)
(*   unit -> *)
(*   unit Lwt.t *)
let rec process_output notify task worker_id proc () =
  let open Lwt_process in
  let* () = Lwt.pause () in
  match proc#state with
  | Running ->
     process_output notify task worker_id proc ()
  | Exited (WEXITED code)
  | Exited (WSIGNALED code)
  | Exited (WSTOPPED code) ->
     let* () =
       if code <> 0 then
         let filename = Filename.basename task in
         let* message =
           let open Lwt in
           Lwt_io.read_lines proc#stderr
           |> Lwt_stream.to_list
           >|= String.concat "\n"
         in
         Lwt_io.with_file
           ~mode:Output
           (Format.sprintf "%s/%s.error" typechecking_results filename)
           (fun chan -> Lwt_io.write chan message)
       else
         return ()
     in
     notify @@ Worker_freed worker_id;
     return ()
