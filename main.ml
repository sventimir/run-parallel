open Core

type worker = {id : int; jobs_limit: int; task : string; progress : progress}

type workers_state = {
    event_stream : event Lwt_stream.t;
    notify : event -> unit;
    jobs : int;
    job_limit : int;
    next_job_id : int;
    progress : progress;
    start_time : int;
  }

let inc {finished; total} = {finished = finished + 1; total}

let is_finished {finished; total} = finished >= total

let pp_progress fmt {finished; total} =
  let percentage = float_of_int (finished * 10000 / total) /. 100. in
  Format.fprintf fmt "%4d / %4d (%6.2f %%)" finished total percentage

let pp_worker fmt {id; task; progress; _} =
  Format.fprintf fmt "Worker #%2d (%-60s): %a" id task pp_progress progress

let pp_cursor_up fmt lns =
  Format.fprintf fmt "\x1B[%dA" lns

let pp_cursor_down fmt lns =
  Format.fprintf fmt "\x1B[%dB" lns

let pp_cursor_forward fmt cols =
  Format.fprintf fmt "\x1B[%dC" cols

let pp_carriage_return fmt () =
  Format.pp_print_string fmt "\x0D"

let pp_worker_progress fmt worker =
  let ln = worker.jobs_limit - worker.id + 1 in
  pp_cursor_up fmt ln;
  pp_worker fmt worker;
  pp_cursor_down fmt ln;
  pp_carriage_return fmt ()

let pp_update_worker_progress fmt worker =
  let ln = worker.jobs_limit - worker.id + 1 in
  pp_cursor_up fmt ln;
  pp_cursor_forward fmt 75;
  pp_progress fmt worker.progress;
  pp_carriage_return fmt ();
  pp_cursor_down fmt ln

let pp_global_progress fmt progress =
  pp_cursor_up fmt 1;
  Format.fprintf fmt "Progress: %a" pp_progress progress;
  pp_carriage_return fmt ();
  pp_cursor_down fmt 1

let pp_elapsed_time fmt time =
  let sec = time mod 60 in
  let min =  (time / 60) mod 60 in
  let hr = time / 3660 in
  Format.fprintf fmt "Elapsed time: %2dh %2dm %2ds.%a" hr min sec pp_carriage_return ()

let print s =
  let* () = Lwt_io.print s in
  Lwt_io.(flush stdout)
  
let print_global_progress progress =
  Format.kasprintf print "%a" pp_global_progress progress

let print_worker_progress worker =
  Format.kasprintf print "%a" pp_worker_progress worker

let update_worker_progress worker =
  Format.kasprintf print "%a" pp_update_worker_progress worker

let print_elapsed_time time =
  Format.kasprintf print "%a" pp_elapsed_time time

let iter_n lim f =
  let rec iter n =
    if n < lim then
      let* () = f n in
      iter (n + 1)
    else
      return ()
  in
  iter 0

module Queue(Job : JOB) = struct
  let rec queue state = function
    | (task :: tasks) when state.jobs < state.job_limit ->
       let proc = Job.spawn_process task in
       Lwt.async @@ Job.process_output state.notify task state.next_job_id proc;
       let state' = {state with jobs = state.jobs + 1; next_job_id = state.next_job_id + 1} in
       let* () =
         print_worker_progress {
             id = state.next_job_id;
             task = Job.show_task task;
             jobs_limit = state.job_limit;
             progress = {finished = 0; total = Job.total_progress}
           }
       in
       queue state' tasks
    | tasks ->
       let* cont = Lwt_stream.next state.event_stream in
       (match cont with
        | Worker_freed next_job_id ->
           let progress = inc state.progress in
           let* () = print_global_progress progress in
           let time = int_of_float @@ Unix.time () in
           let* () = print_elapsed_time (time - state.start_time) in
           if is_finished progress then
             return ()
           else
             queue {state with jobs = state.jobs - 1; next_job_id; progress} tasks
        | Progress (worker_id, progress) ->
           let* () =
             update_worker_progress {
                 id = worker_id;
                 jobs_limit = state.job_limit;
                 task = ""; (* does not matter for update. *)
                 progress;
               }
           in
           let time = int_of_float @@ Unix.time () in
           let* () = print_elapsed_time (time - state.start_time) in
           queue state tasks
       )
end

let main (module Job : JOB) =
  let job_limit = int_of_string Sys.argv.(1) in
  let event_stream, notify = Lwt_stream.create () in
  let* tasks = Job.get_tasks () in
  let* () = Lwt_io.(flush stdout) in
  let state = {
      event_stream;
      notify = (fun e -> notify @@ Some e);
      jobs = 0;
      job_limit;
      next_job_id = 0;
      progress = {finished = 0; total = List.length tasks};
      start_time = int_of_float @@ Unix.time ();
    }
  in
  let module Queue = Queue(Job) in
  (* make space for individual workers' progress. *)
  let* () = Lwt_io.print @@ String.make (state.job_limit + 1) '\n' in
  let* () = print_global_progress state.progress in
  let* () = print_elapsed_time 0 in
  let* () = Queue.queue state tasks in
  Lwt_io.print "\n"

let () = Lwt_main.run @@ main (module Tezos_michelson_typecheck)
